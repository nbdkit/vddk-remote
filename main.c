/* vddk remote nbdkit wrapper
 * Copyright (C) 2013-2022 Red Hat Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of Red Hat nor the names of its contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <getopt.h>
#include <limits.h>
#include <termios.h>
#include <errno.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <libvirt/libvirt.h>
#include <libvirt/virterror.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/uri.h>
#include <libxml/xmlstring.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "string-vector.h"
#include "const-string-vector.h"

#define DEBUG_PARAMS 0          /* set to 1 to debug parameter parsing */

static char *libvirt_uri;       /* -c libvirt URI */
static char *libdir;            /* --libdir path to VDDK */
static char *passwordfile;      /* --password path to password file */
static bool unlink_passwordfile; /* Unlink password file on exit. */
static char *pidfile;           /* --pidfile path to PID file */
static char *thumbprint;        /* --thumbprint for the server */
static char *guest;             /* remote guest name */

/* Parameters interpreted before being passed to nbdkit. */
static int port;                /* --port base port number */
static bool verbose;            /* --verbose */
static char *unixsocket;        /* -U base Unix domain socket */
static const char *run_cmd;     /* --run command */

/* Parameters passed to nbdkit unmodified. */
static string_vector params = empty_vector;

/* The libvirt connection. */
static virConnectPtr conn;

/* Various fields we derive from libvirt. */
static char *server;
static xmlDocPtr guest_doc;
static char *moref;
static string_vector files = empty_vector;

/* PIDs and pidfiles of nbdkit children. */
DEFINE_VECTOR_TYPE(pid_vector, pid_t)
static pid_vector pids = empty_vector;
static string_vector pidfiles = empty_vector;

/* Used by get_line() */
static char *line;
static size_t line_len;

typedef struct termios echo_mode;
static void echo_off (echo_mode *old_mode);
static void echo_restore (const echo_mode *old_mode);

static void check_nbdkit_installed (void);
static void parse_command_line (int argc, char **argv);
static void print_usage (int status);
static void interactive_mode (void);
static void libvirt_connect (void);
static void get_server (void);
static void get_thumbprint (void);
static void get_guest_doc (void);
static void get_moref (void);
static void get_files (void);
static void write_pidfile (void);
static void start_all_nbdkit (void);
static void kill_all_nbdkit (void);
static bool looks_like_libdir (const char *dir);
static void list_guests (void);
static size_t get_line (FILE *infp, const char *msg);
static void handler (int);

int
main (int argc, char *argv[])
{
  /* Check nbdkit + nbdkit-vddk-plugin are installed (nbdkit-probing(1)). */
  check_nbdkit_installed ();

  /* Parse our command line. */
  parse_command_line (argc, argv);

  /* If any required setting is missing then enter interactive mode. */
  if (!libvirt_uri || !libdir || !passwordfile || !guest)
    interactive_mode ();

  assert (libvirt_uri);
  assert (libdir);
  assert (passwordfile);
  assert (guest);

  /* Make sure we have connected to libvirt. */
  libvirt_connect ();

  /* Get the thumbprint. */
  get_thumbprint ();

  /* Get the moref. */
  get_moref ();

  /* Get the disks (source file references). */
  get_files ();

  /* We're now ready to run nbdkit. */
  start_all_nbdkit ();

  /* Write our PID file and wait for a signal. */
  write_pidfile ();
  signal (SIGINT, handler);
  signal (SIGTERM, handler);
  signal (SIGQUIT, handler);
  signal (SIGHUP, handler);
  pause ();

  /* Kill all nbdkit subprocesses. */
  kill_all_nbdkit ();

  /* Free up variables. */
  if (passwordfile) {
    if (unlink_passwordfile)
      unlink (passwordfile);
    free (passwordfile);
  }

  free (libvirt_uri);
  free (libdir);
  if (pidfile) unlink (pidfile);
  free (pidfile);
  free (thumbprint);
  free (guest);
  free (unixsocket);
  string_vector_iter (&params, (void *) free);
  string_vector_reset (&params);
  free (server);
  xmlFreeDoc (guest_doc);
  free (moref);
  string_vector_iter (&files, (void *) free);
  string_vector_reset (&files);
  string_vector_iter (&pidfiles, (void *) unlink);
  string_vector_iter (&pidfiles, (void *) free);
  string_vector_reset (&files);
  free (line);
  if (conn) virConnectClose (conn);

  exit (EXIT_SUCCESS);
}

static void
interactive_mode (void)
{
  ssize_t r;

  printf ("%s: Export disks from a remote VMware server over NBD\n",
          PACKAGE_NAME);

  if (!libvirt_uri)
    printf ("\n"
            "You need to enter the libvirt URI for the VMware ESXi or vCenter\n"
            "server.  This will be similar to:\n"
            "    esx://root@esx-server/?no_verify=1                (for ESXi)\n"
            "    vpx://root@vc-server/data/esx-server/?no_verify=1 (for vCenter)\n"
            "?no_verify=1 allows us to connect without verifying the server's\n"
            "SSL certificate, which is usually necessary.  For more about\n"
            "these URIs, see: https://libvirt.org/drvesx.html\n"
            "\n");
  while (!libvirt_uri) {
    printf ("Enter libvirt URI: ");
    r = get_line (stdin, "libvirt URI");
    if (r >= 6 && (strncmp (line, "esx://", 6) == 0 ||
                   strncmp (line, "vpx://", 6) == 0))
      libvirt_uri = strdup (line);
    else
      printf ("error: This doesn't look like a VMware libvirt URI\n");
  }

  while (!passwordfile) {
    echo_mode orig;
    int fd;

    printf ("Password? ");
    echo_off (&orig);
    r = get_line (stdin, "password");
    echo_restore (&orig);
    printf ("\n");

    passwordfile = strdup ("/tmp/vddkXXXXXX");
    fd = mkstemp (passwordfile);
    if (fd == -1) {
      perror ("mkstemp");
      exit (EXIT_FAILURE);
    }
    if (write (fd, line, r) != r || close (fd) != 0) {
      perror ("write");
      exit (EXIT_FAILURE);
    }
    unlink_passwordfile = true;
  }

  /* Now we have the libvirt URI and password, try connecting to the
   * remote hypervisor.
   */
  libvirt_connect ();

  if (!guest)
    printf ("\n"
            "We need to know the name of the guest (VM) on the VMware server.\n"
            "If you do not know the name, hit ⏎ (Return key) to get a list.\n"
            "\n");
  while (!guest) {
    printf ("Enter guest name (or ⏎ to list): ");
    r = get_line (stdin, "guest");
    if (r == 0) {
      printf ("\n"); fflush (stdout);
      list_guests ();
      printf ("\n");
      continue;
    }
    guest = strdup (line);
  }

  /* XXX Try getting default libdir from nbdkit vddk --dump-plugin. */
  if (!libdir)
    printf ("\n"
            "We need to know where the proprietary VDDK library is located.\n"
            "This is usually a directory ending in \"vmware-vix-disklib-distrib\"\n"
            "You have to download this library, it is not provided with\n"
            "the %s tool.\n"
            "\n",
            PACKAGE_NAME);
  while (!libdir) {
    printf ("Enter libdir: ");
    r = get_line (stdin, "libdir");
    if (r > 0 && looks_like_libdir (line))
      libdir = strdup (line);
    else
      printf ("error: This doesn't look like the VMware VDDK library\n");
  }
}

static void __attribute__((noreturn))
print_usage (int status)
{
  printf ("%s: Export disks from a remote VMware server over NBD\n",
          PACKAGE_NAME);
  printf ("To use this tool interactively, just run \"%s\".\n",
          PACKAGE_NAME);
  printf ("For other options, read the %s(1) man page.\n", PACKAGE_NAME);
  exit (status);
}

static void
echo_off (echo_mode *old_mode)
{
  struct termios temp;

  tcgetattr (STDIN_FILENO, old_mode);
  temp = *old_mode;
  temp.c_lflag &= ~ECHO;
  tcsetattr (STDIN_FILENO, TCSAFLUSH, &temp);
}

static void
echo_restore (const echo_mode *old_mode)
{
  tcsetattr (STDIN_FILENO, TCSAFLUSH, old_mode);
}

/* Note we only have to list parameters here that we specifically want
 * to intercept ourselves.  Any unknown options are added to the
 * 'params' list and passed through to nbdkit unchanged.
 */
enum {
  HELP_OPTION = CHAR_MAX + 1,
  LIBDIR_OPTION,
  PASSWORD_OPTION,
  RUN_OPTION,
  THUMBPRINT_OPTION,
};

static const char *short_options = "c:P:p:U:v";
static const struct option long_options[] = {
  { "connect",        required_argument, NULL, 'c' },
  { "help",           no_argument,       NULL, HELP_OPTION },
  { "libdir",         required_argument, NULL, LIBDIR_OPTION },
  { "password",       required_argument, NULL, PASSWORD_OPTION },
  { "pidfile",        required_argument, NULL, 'P' },
  { "port",           required_argument, NULL, 'p' },
  { "run",            required_argument, NULL, RUN_OPTION },
  { "thumbprint",     required_argument, NULL, THUMBPRINT_OPTION },
  { "unix",           required_argument, NULL, 'U' },
  { "verbose",        no_argument,       NULL, 'v' },
  { NULL }
};

#if DEBUG_PARAMS
static void
print_param (char *param)
{
  printf (" %s", param);
}
#endif

static void
parse_command_line (int argc, char **argv)
{
  int c;

  /* Suppress automatic errors about unknown options. */
  opterr = 0;

  for (;;) {
    c = getopt_long (argc, argv, short_options, long_options, NULL);
    if (c == -1)
      break;

    switch (c) {
    case 'c':
      libvirt_uri = strdup (optarg);
      break;
    case HELP_OPTION:
      print_usage (EXIT_SUCCESS);
    case LIBDIR_OPTION:
      libdir = strdup (optarg);
      break;
    case PASSWORD_OPTION:
      passwordfile = strdup (optarg);
      break;
    case 'P':
      pidfile = strdup (optarg);
      break;
    case 'p':
      if (sscanf (optarg, "%d", &port) != 1) {
        fprintf (stderr, "%s: could not parse port number option: %s\n",
                 PACKAGE_NAME, optarg);
        exit (EXIT_FAILURE);
      }
      break;
    case RUN_OPTION:
      run_cmd = optarg;
      break;
    case THUMBPRINT_OPTION:
      thumbprint = strdup (optarg);
      break;
    case 'U':
      unixsocket = strdup (optarg);
      break;
    case 'v':
      verbose = true;
      break;

    case '?':
      /* Any unknown option is added to the params list to pass it
       * through to nbdkit.
       */
      string_vector_append (&params, strdup (argv[optind-1]));
      break;

    default:
      abort ();
    }
  }

  /* For non-option arguments, the first is the guest name and any
   * remaining ones are passed through to nbdkit.
   */
  if (optind < argc) {
    guest = strdup (argv[optind++]);

    while (optind < argc)
      string_vector_append (&params, strdup (argv[optind++]));
  }

#if DEBUG_PARAMS
  /* In verbose mode print a summary of command line parsing. */
  if (verbose) {
    printf ("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);

    if (libvirt_uri)
      printf ("libvirt URI (from -c):            %s\n", libvirt_uri);
    if (libdir)
      printf ("libdir (from --libdir):           %s\n", libdir);
    if (passwordfile)
      printf ("password file (from --password):  %s\n", passwordfile);
    if (pidfile)
      printf ("PID file (from --pidfile):        %s\n", pidfile);
    if (port > 0)
      printf ("port number (from --port):        %d\n", port);
    if (thumbprint)
      printf ("thumbprint (from --thumbprint):   %s\n", thumbprint);
    if (unixsocket)
      printf ("Unix domain socket (from --unix): %s\n", unixsocket);
    printf ("--run parameter present:          %s\n", run_cmd ? "yes" : "no");
    if (guest)
      printf ("guest name from command line:     %s\n", guest);
    if (params.len) {
      printf ("parameters passed through unchanged to nbdkit:\n   ");
      string_vector_iter (&params, print_param);
      printf ("\n");
    }
  }
#endif
}

/* Does the directory look like it could be a VDDK library path? */
static bool
is_directory (const char *dir)
{
  struct stat statbuf;

  if (stat (dir, &statbuf) == -1) {
    fprintf (stderr, "stat: %s: %s\n", dir, strerror (errno));
    return false;
  }
  if (!S_ISDIR (statbuf.st_mode)) {
    fprintf (stderr, "stat: %s: not a directory\n", dir);
    return false;
  }

  return true;
}

static bool
looks_like_libdir (const char *dir)
{
  char *lib64dir;
  bool r;

  if (!is_directory (dir))
    return false;

  if (asprintf (&lib64dir, "%s/lib64", dir) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  r = is_directory (lib64dir);
  free (lib64dir);
  return r;
}

/* Connect to libvirt. */
static int
do_libvirt_auth (virConnectCredentialPtr cred,
                 unsigned int ncred,
                 void *vp)
{
  const char *password = vp;
  size_t i;

  if (cred == NULL || ncred == 0)
    return 0;

  for (i = 0; i < ncred; ++i) {
    cred[i].result = NULL;
    cred[i].resultlen = 0;
  }

  for (i = 0; i < ncred; ++i) {
    if (cred[i].type == VIR_CRED_PASSPHRASE) {
      cred[i].result = strdup (password);
      cred[i].resultlen = strlen (password);
    }
  }
  return 0;
}

static void
libvirt_connect (void)
{
  FILE *fp;
  virConnectAuth authdata;
  int creds[1] = { VIR_CRED_PASSPHRASE };
  virErrorPtr err;

  /* These must have been set by the caller. */
  assert (libvirt_uri);
  assert (passwordfile);

  if (conn) return;

  if (verbose) {
    printf ("Connecting to VMware server ...\n");
    fflush (stdout);
  }

  /* Read the password into memory. */
  fp = fopen (passwordfile, "r");
  if (fp == NULL) {
    perror (passwordfile);
    exit (EXIT_FAILURE);
  }
  errno = 0;
  get_line (fp, passwordfile);
  fclose (fp);

  /* Set up the libvirt credentials. */
  memset (&authdata, 0, sizeof authdata);
  authdata.credtype = creds;
  authdata.ncredtype = 1;
  authdata.cb = do_libvirt_auth;
  authdata.cbdata = line;

  conn = virConnectOpenAuth (libvirt_uri, &authdata, 0);
  if (conn == NULL) {
    err = virGetLastError ();
    fprintf (stderr, "%s: could not connect to VMware server: %s\n",
             PACKAGE_NAME, err->message);
    exit (EXIT_FAILURE);
  }

  if (verbose)
    printf ("Connected to VMware server.\n");
}

static int
compare_strings (const char **s1p, const char **s2p)
{
  const char *s1 = *s1p;
  const char *s2 = *s2p;
  return strcasecmp (s1, s2);
}

static void
list_guests (void)
{
  size_t i, j, k, len, widest, cols, rows;
  int r;
  virErrorPtr err;
  virDomainPtr *domains;
  const_string_vector names = empty_vector;

  assert (conn);

  r = virConnectListAllDomains (conn, &domains,
                                VIR_CONNECT_LIST_DOMAINS_INACTIVE |
                                VIR_CONNECT_LIST_DOMAINS_PERSISTENT);
  if (r < 0) {
    err = virGetLastError ();
    fprintf (stderr, "%s: could not list VMware guests: %s\n",
             PACKAGE_NAME, err->message);
    exit (EXIT_FAILURE);
  }

  /* Get the widest domain name.  XXX Internationalization! */
  widest = 0;
  for (i = 0; i < (size_t) r; ++i) {
    const char *name = virDomainGetName (domains[i]);
    if (name) {
      const_string_vector_append (&names, name);
      len = strlen (name);
      if (len > widest) widest = len;
    }
  }

  const_string_vector_sort (&names, compare_strings);

  /* Print them in columns if we can. */
  widest += 2;
  cols = 80 / widest;
  if (cols < 1) cols = 1;
  rows = names.len / cols;
  if (rows * cols < names.len) rows++;
  assert (rows * cols >= names.len);

  for (i = 0; i < rows; ++i) {
    for (j = 0; j < cols; ++j) {
      k = i + j*rows;
      if (k < names.len)
        printf ("%-*s", (int) widest, names.ptr[k]);
    }
    printf ("\n");
  }

  /* Free the domains and names. */
  for (i = 0; i < (size_t) r; ++i)
    virDomainFree (domains[i]);
  free (domains);

  const_string_vector_reset (&names);
}

/* A wrapper around getline which has some awkward error cases.  This
 * always removes the final '\n' character.  Returns the number of
 * characters read.  The actual line returned is in the global
 * variable 'line'.
 */
static size_t chomp (void);

static size_t
get_line (FILE *fp, const char *msg)
{
  ssize_t r;

  errno = 0;
  r = getline (&line, &line_len, fp);
  if (r == -1) {
    if (errno) perror (msg);
    else
      fprintf (stderr, "%s: %s: unexpected end of file in input\n",
               PACKAGE_NAME, msg);
    exit (EXIT_FAILURE);
  }
  return chomp ();
}

static size_t
chomp (void)
{
  size_t n = strlen (line);

  if (n > 0 && line[n-1] == '\n') {
    line[n-1] = '\0';
    n--;
  }
  return n;
}

/* Parse the server name/IP address out of the libvirt URI. */
static void
get_server (void)
{
  xmlURIPtr uri;

  if (server) return;

  assert (libvirt_uri);

  uri = xmlParseURI (libvirt_uri);
  if (!uri) {
    fprintf (stderr, "%s: failed to parse libvirt URI\n", PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }

  if (!uri->scheme ||
      (strcmp (uri->scheme, "esx") != 0 && strcmp (uri->scheme, "vpx") != 0)) {
    fprintf (stderr, "%s: libvirt URI is not esx://.. or vpx://..: %s\n",
             PACKAGE_NAME, libvirt_uri);
    exit (EXIT_FAILURE);
  }

  if (!uri->server) {
    fprintf (stderr, "%s: libvirt URI missing server field: %s\n",
             PACKAGE_NAME, libvirt_uri);
    exit (EXIT_FAILURE);
  }

  server = strdup (uri->server);

  if (verbose)
    printf ("server: %s\n", server);

  xmlFreeURI (uri);
}

/* Query the remote server for the thumbprint. */
#if 0

/* In theory it should be possible with curl, but for unclear reasons
 * the fingerprint is not returned by CURLOPT_CERTINFO.
 */

static void
get_thumbprint (void)
{
  char *url;
  CURL *curl;
  CURLcode r;
  struct curl_certinfo *ci;
  struct curl_slist *slist;
  size_t i;

  if (thumbprint) return;

  get_server ();

  /* Connect to the remote server on port 443 and tell libcurl to save
   * the server certificates.
   */
  curl = curl_easy_init ();
  if (!curl) {
    perror ("curl_easy_init");
    exit (EXIT_FAILURE);
  }

  if (asprintf (&url, "https://%s:443/", server) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  curl_easy_setopt(curl, CURLOPT_URL, url);
  free (url);

  //curl_easy_setopt(curl, CURLOPT_PORT, 443L);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
  curl_easy_setopt(curl, CURLOPT_CERTINFO, 1L);

  r = curl_easy_perform (curl);
  if (r != CURLE_OK) {
    fprintf (stderr, "%s: could not get thumbprint from %s:443.\n"
             "Use the --thumbprint option, and/or read nbdkit-vddk-plugin(1)\n"
             "section \"THUMBPRINTS\".\n",
             PACKAGE_NAME, server);
    exit (EXIT_FAILURE);
  }

  r = curl_easy_getinfo (curl, CURLINFO_CERTINFO, &ci);
  if (r != CURLE_OK) {
    fprintf (stderr, "%s: could not get SSL certificates from %s:443.\n"
             "Use the --thumbprint option, and/or read nbdkit-vddk-plugin(1)\n"
             "section \"THUMBPRINTS\".\n",
             PACKAGE_NAME, server);
    exit (EXIT_FAILURE);
  }

  if (verbose) {
    printf ("certificates from %s:\n", server);
    for (i = 0; i < ci->num_of_certs; ++i) {
      for (slist = ci->certinfo[i]; slist; slist = slist->next)
        printf ("%s\n", slist->data);
    }
  }

  curl_easy_cleanup (curl);
}

#endif /* 0 */

/* This method to get the thumbprint just runs the openssl command
 * line tool.  It should be possible to do this programmatically XXX.
 */
static void
get_thumbprint (void)
{
  char *command;
  FILE *pp;
  size_t r;

  if (thumbprint) return;
  get_server ();
  assert (server);

  if (asprintf (&command,
                "%s s_client -connect \"%s:443\" </dev/null 2>/dev/null |"
                "%s x509 -in /dev/stdin -fingerprint -sha1 -noout 2>/dev/null |"
                "grep -i '^sha1 Fingerprint=' |"
                "sed 's/.*Fingerprint=\\([A-F0-9:]\\+\\)/\\1/'",
                OPENSSL, server, OPENSSL) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }

  pp = popen (command, "r");
  if (pp == NULL) {
    perror ("popen: openssl");
    exit (EXIT_FAILURE);
  }

  r = get_line (pp, command);
  pclose (pp);
  free (command);

  if (r == 0) {
    fprintf (stderr, "%s: could not read thumbprint from %s:443.\n"
             "Use the --thumbprint option, and/or read nbdkit-vddk-plugin(1)\n"
             "section \"THUMBPRINTS\".\n",
             PACKAGE_NAME, server);
    exit (EXIT_FAILURE);
  }

  if (verbose)
    printf ("thumbprint: %s\n", line);

  thumbprint = strdup (line);
}

/* Get the libvirt XML and parse fields out of it. */
static void
get_guest_doc (void)
{
  virDomainPtr dom;
  char *xml;
  virErrorPtr err;

  if (guest_doc) return;

  libvirt_connect ();
  assert (conn);
  assert (guest);

  if (verbose) {
    printf ("Fetching guest metadata from VMware server ...\n");
    fflush (stdout);
  }

  dom = virDomainLookupByName (conn, guest);
  if (!dom) {
    err = virGetLastError ();
    fprintf (stderr, "%s: could not find guest %s: %s\n",
             PACKAGE_NAME, guest, err->message);
    exit (EXIT_FAILURE);
  }

  xml = virDomainGetXMLDesc (dom, VIR_DOMAIN_XML_INACTIVE);
  if (!xml) {
    err = virGetLastError ();
    fprintf (stderr, "%s: could not read libvirt XML of guest %s: %s\n",
             PACKAGE_NAME, guest, err->message);
    exit (EXIT_FAILURE);
  }

  guest_doc = xmlReadMemory (xml, strlen (xml),
                             NULL, NULL, XML_PARSE_NONET);
  if (guest_doc == NULL) {
    fprintf (stderr, "%s: could not parse the XML returned by libvirt\n",
             PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }

  free (xml);
  virDomainFree (dom);
}

static void
get_moref (void)
{
  xmlXPathContextPtr xpathCtx;
  xmlXPathObjectPtr xpathObj;
  xmlNodePtr node;

  get_guest_doc ();
  assert (guest_doc);

  xpathCtx = xmlXPathNewContext (guest_doc);
  if (xpathCtx == NULL) {
    fprintf (stderr, "%s: unable to create new XPath context\n",
             PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }

  if (xmlXPathRegisterNs (xpathCtx,
                          BAD_CAST "vmware",
                          BAD_CAST "http://libvirt.org/schemas/domain/vmware/1.0") == -1) {
    fprintf (stderr, "%s: unable to register vmware namespace\n",
             PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }
  xpathObj = xmlXPathEvalExpression (BAD_CAST "/domain/vmware:moref", xpathCtx);
  if (!xpathObj || xpathObj->nodesetval == NULL ||
      xpathObj->nodesetval->nodeNr < 1) {
    fprintf (stderr, "%s: cannot evaluate xpath moref expression\n",
             PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }

  node = xpathObj->nodesetval->nodeTab[0];
  switch (node->type) {
  case XML_TEXT_NODE:
  case XML_COMMENT_NODE:
  case XML_CDATA_SECTION_NODE:
  case XML_PI_NODE:
    moref = strdup ((char *) node->content);
    break;
  case XML_ATTRIBUTE_NODE:
  case XML_ELEMENT_NODE:
    moref = (char *) xmlNodeListGetString (guest_doc, node->children, 1);
    break;

  default:
    fprintf (stderr, "%s: <vmware:moref> node is not a string\n",
             PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }

  xmlXPathFreeObject (xpathObj);
  xmlXPathFreeContext (xpathCtx);

  if (verbose)
    printf ("moref: %s\n", moref);
}

static void
get_files (void)
{
  xmlXPathContextPtr xpathCtx;
  xmlXPathObjectPtr xpathObj;
  size_t i;
  xmlNodePtr node;

  get_guest_doc ();
  assert (guest_doc);

  xpathCtx = xmlXPathNewContext (guest_doc);
  if (xpathCtx == NULL) {
    fprintf (stderr, "%s: unable to create new XPath context\n",
             PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }

  xpathObj = xmlXPathEvalExpression (BAD_CAST "/domain/devices/disk[@device='disk']/source/@file",
                                     xpathCtx);
  if (!xpathObj || xpathObj->nodesetval == NULL ||
      xpathObj->nodesetval->nodeNr < 1) {
    fprintf (stderr, "%s: cannot evaluate xpath files expression\n",
             PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }

  for (i = 0; i < xpathObj->nodesetval->nodeNr; ++i) {
    node = xpathObj->nodesetval->nodeTab[i];
    switch (node->type) {
    case XML_TEXT_NODE:
    case XML_COMMENT_NODE:
    case XML_CDATA_SECTION_NODE:
    case XML_PI_NODE:
      string_vector_append (&files, strdup ((char *) node->content));
      break;
    case XML_ATTRIBUTE_NODE:
    case XML_ELEMENT_NODE:
      string_vector_append (&files,
                            (char *) xmlNodeListGetString (guest_doc,
                                                           node->children, 1));
      break;

    default:
      fprintf (stderr, "%s: <source file> attribute is not a string\n",
               PACKAGE_NAME);
      exit (EXIT_FAILURE);
    }
  }

  xmlXPathFreeObject (xpathObj);
  xmlXPathFreeContext (xpathCtx);

  if (verbose) {
    for (i = 0; i < files.len; ++i)
      printf ("file[%zu]: \"%s\"\n", i, files.ptr[i]);
  }
}

/* Write our pidfile. */
static void
write_pidfile (void)
{
  FILE *fp;

  if (!pidfile) return;

  fp = fopen (pidfile, "w");
  if (fp == NULL) {
    perror (pidfile);
    exit (EXIT_FAILURE);
  }
  fprintf (fp, "%d", (int) getpid ());
  fclose (fp);
}

/* Check nbdkit + nbdkit-vddk-plugin are installed (nbdkit-probing(1)). */
static void
check_nbdkit_installed (void)
{
  char *cmd;

  if (asprintf (&cmd, "%s --dump-config >/dev/null", NBDKIT) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  if (system (cmd) != 0) {
    fprintf (stderr, "%s: nbdkit is not installed\n", PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }
  free (cmd);

  if (asprintf (&cmd, "%s vddk --dump-plugin >/dev/null", NBDKIT) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  if (system (cmd) != 0) {
    fprintf (stderr, "%s: nbdkit-vddk-plugin is not installed\n", PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }
  free (cmd);
}

/* Start up nbdkit instances, one per disk. */
static void
start_one_nbdkit (size_t n)
{
  string_vector args = empty_vector;
  size_t i;
  char *str;
  pid_t pid;
  char *pidfile;
  int fd;

  assert (moref);
  assert (server);
  assert (thumbprint);

  string_vector_append (&args, strdup (NBDKIT));
  string_vector_append (&args, strdup ("--exit-with-parent"));
  string_vector_append (&args, strdup ("-f"));
  if (verbose)
    string_vector_append (&args, strdup ("-v"));

  /* The port number or Unix socket that nbdkit serves on. */
  if (unixsocket) {
    if (n == 0) {
      if (asprintf (&str, "--unix=%s", unixsocket) == -1) {
        perror ("asprintf");
        exit (EXIT_FAILURE);
      }
    }
    else {
      if (asprintf (&str, "--unix=%s.%zu", unixsocket, n) == -1) {
        perror ("asprintf");
        exit (EXIT_FAILURE);
      }
    }
    string_vector_append (&args, str);
  }
  else {
    if (!port)
      port = 10809;
    if (asprintf (&str, "--port=%d", (int) (port+n)) == -1) {
      perror ("asprintf");
      exit (EXIT_FAILURE);
    }
    string_vector_append (&args, str);
  }

  /* Tell nbdkit to write a PID file.  This is so we can tell if
   * nbdkit started up.
   */
  pidfile = strdup ("/tmp/nbdkitpidXXXXXX");
  fd = mkstemp (pidfile);
  if (fd == -1) {
    perror ("mkstemp");
    exit (EXIT_FAILURE);
  }
  close (fd);
  unlink (pidfile);
  string_vector_append (&pidfiles, pidfile);

  if (asprintf (&str, "--pidfile=%s", pidfile) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  string_vector_append (&args, str);

  /* Plugin name & file parameter. */
  string_vector_append (&args, strdup ("vddk"));
  if (asprintf (&str, "file=%s", files.ptr[n]) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  string_vector_append (&args, str);

  /* libdir parameter. */
  if (asprintf (&str, "libdir=%s", libdir) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  string_vector_append (&args, str);

  /* The parameters we've derived from libvirt. */
  if (asprintf (&str, "vm=moref=%s", moref) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  string_vector_append (&args, str);
  if (asprintf (&str, "server=%s", server) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  string_vector_append (&args, str);
  if (asprintf (&str, "thumbprint=%s", thumbprint) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  string_vector_append (&args, str);

  /* XXX Let the username be selectable. */
  string_vector_append (&args, strdup ("user=root"));

  if (asprintf (&str, "password=+%s", passwordfile) == -1) {
    perror ("asprintf");
    exit (EXIT_FAILURE);
  }
  string_vector_append (&args, str);

  /* Copy the extra parameters passed in on the command line. */
  for (i = 0; i < params.len; ++i)
    string_vector_append (&args, strdup (params.ptr[i]));

  /* Run command? */
  if (run_cmd) {
    string_vector_append (&args, strdup ("--run"));
    string_vector_append (&args, strdup (run_cmd));
  }

  /* NULL-terminate the command list for exec. */
  string_vector_append (&args, NULL);

  if (verbose) {
    for (i = 0; i < args.len - 1 /* ignore NULL */; ++i) {
      if (i > 0) printf (" ");
      if (strchr (args.ptr[i], ' ') != NULL)
        printf ("'%s'", args.ptr[i]);
      else
        printf ("%s", args.ptr[i]);
    }
    printf ("\n");
  }

  /* Run nbdkit. */
  pid = fork ();
  if (pid == -1) {
    perror ("fork");
    exit (EXIT_FAILURE);
  }
  if (pid == 0) {
    execvp (NBDKIT, args.ptr);
    perror (NBDKIT);
    _exit (EXIT_FAILURE);
  }
  pid_vector_append (&pids, pid);

  /* Free command line args for this instance. */
  string_vector_iter (&args, (void *) free);
  string_vector_reset (&args);
}

static void
start_all_nbdkit (void)
{
  size_t n, i;

  if (run_cmd && files.len > 1) {
    fprintf (stderr, "%s: cannot use --run for a guest with multiple disks\n",
             PACKAGE_NAME);
    exit (EXIT_FAILURE);
  }

  if (verbose) {
    printf ("Starting nbdkit ...\n");
    fflush (stdout);
  }

  for (n = 0; n < files.len; ++n)
    start_one_nbdkit (n);

  assert (pids.len == files.len);
  assert (pidfiles.len == files.len);

  /* Wait for nbdkit instances to write a PID file.
   *
   * nbdkit-vddk-plugin doesn't actually connect to VMware until
   * something connects to nbdkit, so we're not waiting for VMware
   * here.
   */
  for (n = 0; n < pidfiles.len; ++n) {
    for (i = 0; i < 60; ++i) {
      if (access (pidfiles.ptr[n], R_OK) == 0)
        break;
      if (waitpid (pids.ptr[n], NULL, WNOHANG) == pids.ptr[n]) {
        pids.ptr[n] = 0;
        goto failed;
      }
      sleep (1);
    }
    if (i == 60) {
    failed:
      fprintf (stderr, "%s: nbdkit instance did not start, check earlier errors\n",
               PACKAGE_NAME);
      exit (EXIT_FAILURE);
    }
  }
}

/* Kill all our nbdkit subprocesses and wait. */
static void
kill_all_nbdkit (void)
{
  size_t n;
  pid_t r;

  for (n = 0; n < pids.len; ++n) {
    if (pids.ptr[n] > 1) {
      kill (pids.ptr[n], SIGINT);
    }
  }

  for (n = 0; n < pids.len; ++n) {
    if (pids.ptr[n] > 1) {
      r = waitpid (pids.ptr[n], NULL, 0);
      if (r == -1)
        perror ("waitpid");
    }
  }
}

/* We just need this function so pause() will work. */
static void
handler (int sig)
{
  /* nothing */
}
