=encoding utf-8

=head1 NAME

vddk-remote - Export disks from a remote VMware server over NBD

=head1 SYNOPSIS

 vddk-remote

 vddk-remote [-c 'esx://esx-server?no_verify=1' |
              -c 'vpx://vcenter-server/data/esx-server?no_verify=1' ]
             --libdir=/path/to/vmware-vix-disklib-distrib
             --password=/tmp/passwordfile
             [--thumbprint=aa:bb:cc...]
             [--pidfile=PIDFILE] [--port=PORT] [--unix=SOCKET]
             [-r|--readonly] [other nbdkit options ...]
             GUEST_NAME

=head1 DESCRIPTION

Vddk-remote is a tool for exporting the disks from a guest (virtual
machine) stored on a remote VMware server as local NBD exports.  It
uses L<nbdkit(1)> and L<nbdkit-vddk-plugin(1)> and is essentially an
easy to use wrapper around those tools.  It requires VMware's
proprietary VDDK library, which you must provide separately.

  guest
  disks        ┌────────────────┐             ┌────────────────┐
  exposed      │ vddk-remote    │             │ VMware server  │
  over     ╍╍╍╍▶ nbdkit      ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍▶  guest       │
  open     ╍╍╍╍▶ nbdkit         │ proprietary │                │
  NBD          └────────────────┘ VDDK        └────────────────┘
  protocol

                     data transfer direction
                 🞀╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍   reading over NBD
                  ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍▶  writing over NBD

=head2 Examples

If you just run vddk-remote on its own then it will take you through a
set of interactive questions:

 $ vddk-remote
 Enter libvirt URI: esx://esx-server?no_verify=1
 [...]

The command exposes NBD endpoints (by default the first disk on port
10809, the second disk on port 10810, etc.)  You can connect to these
using various tools, eg:

 $ nbdsh -u nbd://localhost
 $ guestfish --ro --format=raw -a nbd://localhost -i
 $ mkdir dir && nbdfuse dir nbd://localhost
 # nbd-client localhost /dev/nbd0

See L<nbdsh(1)>, L<guestfish(1)>, L<nbdfuse(1)> and
L<nbdkit-client(1)> for more information about these commands.

Expose the disk(s) of a guest on a VMware ESXi server over NBD ports
10809 and up:

 $ echo mysecret > /tmp/passwordfile
 $ vddk-remote -c 'esx://esx-server?no_verify=1' \
               --libdir=/path/to/vmware-vix-disklib-distrib \
               --password=/tmp/passwordfile \
               guest_name

The vddk-remote command stays in the foreground until killed, and any
nbdkit servers exit at the same time.

The same, for a VMware vCenter server, read-only, over Unix domain
sockets, with caching, and connect to the guest with L<guestfish(1)>:

 $ vddk-remote -c 'vpx://vc-server/data/esx-server?no_verify=1' \
               --libdir=/path/to/vmware-vix-disklib-distrib \
               --password=/tmp/passwordfile \
               --unix=/tmp/sock \
               -r guest_with_2_disks \
               --filter=cache cache-on-read=true
 $ guestfish --ro --format=raw \
             -a 'nbd://?socket=/tmp/sock' \
             -a 'nbd://?socket=/tmp/sock.1' -i

=head1 OPTIONS

=over 4

=item B<-c> URI

=item B<--connect=>URI

Specify the remote VMware server.  Use a libvirt-compatible URI,
defined on this page: L<https://libvirt.org/drvesx.html>

=item B<--libdir=>/path/to/vmware-vix-disklib-distrib

Provide the path to the VMware proprietary VDDK library.

=item B<--password=>PASSWORD_FILE

Specify a file containing the password needed to access the remote
server.

=item B<-P> PIDFILE

=item B<--pidfile=>PIDFILE

Write the process ID of vddk-remote to C<PIDFILE> after all nbdkit
instances are ready.

=item B<--thumbprint=>THUMBPRINT

The thumbprint is required to authenticate VDDK connections against
the VMware server.  Usually vddk-remote can get the thumbprint
automatically so you I<should not> need to specify this.  Only use
this if the automatic detection in vddk-remote fails.

See also: L<nbdkit(1)/THUMBPRINTS>

=back

Other options are passed through to L<nbdkit(1)>.  This discusses the
most interesting ones:

=over 4

=item B<--filter=>FILTER

Add a filter in front of L<nbdkit-vddk-plugin(1)>.  If the VMware
server is slow, a useful filter to use is L<nbdkit-cache-filter(1)>,
like this:

 vddk-remote guest --filter=cache cache-on-read=true

=item B<-p> PORT

=item B<--port=>PORT

Set the port number that nbdkit listens on.  If there are multiple
disks, then multiple copies of nbdkit run, listening on C<PORT>,
C<PORT+1>, C<PORT+2> etc.

=item B<-r>

=item B<--readonly>

Export the disk read-only.

=item B<--run=>CMD

Run a command as a captive subprocess of nbdkit.  This only works if
the guest has a single disk (so only one nbdkit instance is running),
otherwise using this option will return an error.

=item B<-U> SOCKET

=item B<--unix=>SOCKET

Set the Unix domain socket that nbdkit listens on.  If there are
multiple disks, then multiple copies of nbdkit run, listening on
C<SOCKET>, C<SOCKET.1>, C<SOCKET.2> etc.

=item B<-v>

=item B<--verbose>

Print verbose messages.

=back

=head1 SEE ALSO

L<nbdkit(1)>,
L<nbdkit-vddk-plugin(1)>,
L<nbdkit-cache-filter(1)>,
L<nbdkit-client(1)>,
L<guestfish(1)>,
L<nbdfuse(1)>,
L<nbdsh(1)>,
L<nbd-client(8)>,
L<https://libvirt.org/>.

=head1 AUTHOR

Richard W.M. Jones

=head1 COPYRIGHT

Copyright (C) 2013-2022 Red Hat Inc.

=head1 LICENSE

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

=over 4

=item *

Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

=item *

Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

=item *

Neither the name of Red Hat nor the names of its contributors may be
used to endorse or promote products derived from this software without
specific prior written permission.

=back

THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
